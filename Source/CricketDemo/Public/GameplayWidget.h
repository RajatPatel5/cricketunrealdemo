// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include <Components/Checkbox.h>
#include "GameplayWidget.generated.h"

/**
 * 
 */
UCLASS()
class CRICKETDEMO_API UGameplayWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	virtual void NativeConstruct() override;

public:
	bool isLeftSide;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UCheckBox* sideCheckbox;

	UFUNCTION(BlueprintCallable, meta = (BindWidget))
		void SwitchBatSide(bool isLeft);

	DECLARE_EVENT_OneParam(UGameplayWidget, OnSideChanged, bool)
		OnSideChanged onSideChangedEvent;
};
