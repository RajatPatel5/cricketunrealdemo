// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PitchData.generated.h"

UCLASS()
class CRICKETDEMO_API APitchData : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APitchData();

public:
	UPROPERTY(EditAnywhere)
		float pitchLength{};
	UPROPERTY(EditAnywhere)
		float pitchWidth{};
	UPROPERTY(EditAnywhere)
		FVector pitchCenter{};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


};
