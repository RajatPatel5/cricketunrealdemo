// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <GameplayWidget.h>
#include "UIManager.generated.h"

UCLASS()
class CRICKETDEMO_API AUIManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUIManager();

	UGameplayWidget* gameplayWidget{};

protected:
	UPROPERTY(EditAnywhere)
		TSubclassOf<UGameplayWidget> BP_gameplayWidget{};
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
