// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Components/BoxComponent.h>
#include <InputPawn.h>

#include "CricketBat.generated.h"

UCLASS()
class CRICKETDEMO_API ACricketBat : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACricketBat();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
		UBoxComponent* triggerAreaBoxComponent;
	UPROPERTY(EditAnywhere)
		UBoxComponent* triggerAreaIgnoreBoxComponent;
	UPROPERTY(EditAnywhere)
		USceneComponent* batPivot;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* batMesh;

	UPROPERTY(EditAnywhere)
		AInputPawn* myInputPawn;

	UPROPERTY(EditAnywhere)
		UClass* spawningActorBP;

	UFUNCTION()
		void OnFrontAreaTriggered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBatHittedWithBall(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBackAreaTriggered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void RegisterEvents();
	UFUNCTION()
		void DeregisterEvents();
	UFUNCTION()
		void OnBallSpawned();

private:
	bool isHittingBall = false;
	FVector ballHitLocation{};
	UPROPERTY(EditAnywhere)
		float ballRotateSpeed = 10;

	FRotator initialRotation;

	UStaticMeshComponent* hittedBall;

private:
	FScriptDelegate frontAreaTrigger;
	FScriptDelegate backAreaTrigger;
	FScriptDelegate batBallHit;

};
