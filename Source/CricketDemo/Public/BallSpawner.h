// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InputPawn.h"
#include <PitchData.h>
#include <Components/Widget.h>
#include <Components/Button.h>
#include <GameplayWidget.h>
#include <UIManager.h>

#include "BallSpawner.generated.h"


UCLASS()
class CRICKETDEMO_API ABallSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABallSpawner();
	UPROPERTY(EditAnywhere)
		bool isBallSpawned;

	UPROPERTY(EditAnywhere)
		USceneComponent* ballSpawnLocation;

	UPROPERTY(EditAnywhere)
		UClass* BP_Ball;

	UPROPERTY(EditAnywhere)
		APitchData* pitchData;

	UPROPERTY(EditAnywhere)
		UClass* tempSpawnLoc;

	UPROPERTY(EditAnywhere)
		AUIManager* uiManager;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
		UButton* batsmanSideCheckbox;

	UPROPERTY(EditAnywhere)
		float velocity;

public:
	UPROPERTY(EditAnywhere)
		AInputPawn* myInputPawn;
public:
	UFUNCTION()
		void SpawnBall();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	AActor* spawnedBall;

private:
	FVector GenerateRandomPointInPitch();
};
