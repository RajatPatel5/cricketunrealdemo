// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CricketDemoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CRICKETDEMO_API ACricketDemoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
