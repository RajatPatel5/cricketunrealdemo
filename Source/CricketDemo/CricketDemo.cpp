// Copyright Epic Games, Inc. All Rights Reserved.

#include "CricketDemo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CricketDemo, "CricketDemo" );
