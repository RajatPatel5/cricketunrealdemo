// Fill out your copyright notice in the Description page of Project Settings.


#include "BallSpawner.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
ABallSpawner::ABallSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isBallSpawned = false;

	ballSpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Spawn Location"));

	spawnedBall = nullptr;
	pitchData = nullptr;
}

// Called when the game starts or when spawned
void ABallSpawner::BeginPlay()
{
	Super::BeginPlay();

	myInputPawn->BPressedEvent.AddLambda([this]() { ABallSpawner::SpawnBall(); });
}

// Called every frame	
void ABallSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABallSpawner::SpawnBall()
{

	isBallSpawned = !isBallSpawned;
	FTransform loc = ballSpawnLocation->GetComponentTransform();

	UStaticMeshComponent* MeshRootComp;
	if (spawnedBall != nullptr)
	{
		MeshRootComp = Cast<UStaticMeshComponent>(spawnedBall->GetComponentByClass(UStaticMeshComponent::StaticClass()));
		MeshRootComp->SetWorldLocation(loc.GetLocation());
		MeshRootComp->SetRelativeRotation(FQuat());
		MeshRootComp->SetAllPhysicsLinearVelocity(FVector(0, 0, 0));
		MeshRootComp->SetAllPhysicsAngularVelocityInDegrees(FVector(0, 0, 0));
	}
	else
	{
		spawnedBall = GetWorld()->SpawnActorDeferred<AActor>(BP_Ball, loc);
		UGameplayStatics::FinishSpawningActor(spawnedBall, loc);

		MeshRootComp = Cast<UStaticMeshComponent>(spawnedBall->GetComponentByClass(UStaticMeshComponent::StaticClass()));

	}

	float forceForBall = MeshRootComp->GetMass() * (velocity * 10 / 3.6f);
	FVector ballForceVector = GenerateRandomPointInPitch() - loc.GetLocation();
	ballForceVector.Normalize();
	MeshRootComp->AddForce(ballForceVector * forceForBall * 1000);
}

FVector ABallSpawner::GenerateRandomPointInPitch()
{
	float halfPitchLength = (pitchData->pitchLength / 2) - 2;
	float halfPitchWidth = (pitchData->pitchWidth / 2) - 0.5f;

	int sideMultiplier = uiManager->gameplayWidget->isLeftSide ? -1 : 1;

	float lengthOffset = FMath::FRandRange(-halfPitchLength, halfPitchLength);
	float widthOffset = FMath::FRandRange(0, halfPitchWidth * sideMultiplier);

	FVector randomPosition = pitchData->pitchCenter;


	randomPosition.X += lengthOffset * 100;
	randomPosition.Y += widthOffset * 100;


	//Code To Check Ball Bounce Point
	/*FTransform randomLoc = ballSpawnLocation->GetComponentTransform();
	randomLoc.SetLocation(randomPosition);
	GEngine->AddOnScreenDebugMessage(-1, 60, FColor::Red, TEXT("Location ") + randomLoc.GetLocation().ToString());

	auto tempLoc = GetWorld()->SpawnActorDeferred<AActor>(tempSpawnLoc, randomLoc);
	UGameplayStatics::FinishSpawningActor(tempLoc, randomLoc);*/

	return randomPosition;
}

