// Fill out your copyright notice in the Description page of Project Settings.


#include "InputPawn.h"

// Sets default values
AInputPawn::AInputPawn()
{
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	toggle = false;

}

// Called when the game starts or when spawned
void AInputPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AInputPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AInputPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("BallThrow", IE_Pressed, this, &AInputPawn::BroadcastBPressed);
}


void AInputPawn::InputDetected()
{
	toggle = !toggle;
}
