// Fill out your copyright notice in the Description page of Project Settings.


#include "CricketBat.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>

// Sets default values
ACricketBat::ACricketBat()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	auto root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));


	triggerAreaBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerArea"));
	triggerAreaIgnoreBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerAreaIgnore"));
	batPivot = CreateDefaultSubobject<USceneComponent>(TEXT("BatPivot"));
	batMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bat"));

	triggerAreaBoxComponent->SetupAttachment(root);
	triggerAreaIgnoreBoxComponent->SetupAttachment(root);

	batPivot->SetupAttachment(root);
	batMesh->SetupAttachment(batPivot);

	hittedBall = nullptr;
}

// Called when the game starts or when spawned
void ACricketBat::BeginPlay()
{
	Super::BeginPlay();

	frontAreaTrigger.BindUFunction(this, "OnFrontAreaTriggered");
	backAreaTrigger.BindUFunction(this, "OnBackAreaTriggered");
	batBallHit.BindUFunction(this, "OnBatHittedWithBall");

	initialRotation = batPivot->GetComponentRotation();

	myInputPawn->BPressedEvent.AddLambda([this]() { ACricketBat::OnBallSpawned(); });
}

// Called every frame
void ACricketBat::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isHittingBall)
	{
		FVector lookAtLoc = hittedBall->GetComponentLocation();
		lookAtLoc.X = batPivot->GetComponentLocation().X;
		ballHitLocation = lookAtLoc;

		FVector finalBatForward = ballHitLocation - batPivot->GetComponentLocation();
		finalBatForward.Normalize();

		FRotator targetRotation = FRotationMatrix::MakeFromXZ(finalBatForward, FVector(0,1,0)).Rotator();

		//batPivot->SetWorldRotation(FMath::Lerp(batPivot->GetComponentRotation(), targetRotation, ballRotateSpeed * DeltaTime));
		batPivot->SetWorldRotation(targetRotation);
	}
}


void ACricketBat::OnFrontAreaTriggered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	//Code To Check Ball Bounce Point
	UStaticMeshComponent* otherActorStaticMesh = Cast<UStaticMeshComponent>(OtherActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));

	hittedBall = otherActorStaticMesh;

	isHittingBall = true;
}

void ACricketBat::OnBatHittedWithBall(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Bat hit ball "));
	isHittingBall = false;
	DeregisterEvents();
}

void ACricketBat::OnBackAreaTriggered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	isHittingBall = false;
	DeregisterEvents();
}

void ACricketBat::RegisterEvents()
{
	triggerAreaBoxComponent->OnComponentBeginOverlap.Add(frontAreaTrigger);

	triggerAreaIgnoreBoxComponent->OnComponentBeginOverlap.Add(backAreaTrigger);

	batMesh->OnComponentHit.Add(batBallHit);
}
void ACricketBat::DeregisterEvents()
{
	triggerAreaBoxComponent->OnComponentBeginOverlap.Remove(frontAreaTrigger);

	triggerAreaIgnoreBoxComponent->OnComponentBeginOverlap.Remove(backAreaTrigger);

	batMesh->OnComponentHit.Remove(batBallHit);
}

void ACricketBat::OnBallSpawned()
{
	RegisterEvents();
}
