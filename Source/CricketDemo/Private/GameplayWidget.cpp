// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayWidget.h"


void UGameplayWidget::NativeConstruct()
{
	Super::NativeConstruct();

}

void UGameplayWidget::SwitchBatSide(bool isLeft)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Side Changed "));

	UE_LOG(LogTemp, Warning, TEXT("The boolean value is %s"), (isLeft ? TEXT("true") : TEXT("false")));
	this->isLeftSide = isLeft;
	onSideChangedEvent.Broadcast(isLeft);
}